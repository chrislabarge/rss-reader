class FeedCategoriesController < ApplicationController
 def index
    @collection = FeedCategory.all
  end

  def new
    @model = FeedCategory.new
  end

  def create
    @model = FeedCategory.new(resource_params)

    @model.save ? successful_create : unsucessful_create
  end

  def edit
    load_model
  end

  def update
    load_model

    @model.save ? successful_update : unsucessful_update
  end

  def destroy
    load_model
  end

  def show
    load_model
  end

  private

  def resource_params
    params.require(:feed_category).permit(:name, :place)
  end

  def load_model
    @model = FeedCategory.find(params[:id])
  end

  def successful_create
    flash.now[:success] = "Successfully created the Feed Category"

    redirect_to feed_categories_path
  end

  def unsuccessful_create
    flash.now[:success] = "Unable to create the Feed Category"

    redirect_to feed_categories_path
  end

  def successful_update
    flash.now[:success] = "Successfully updated the Feed Category"

    redirect_to feed_category_path @model
  end

  def unsuccessful_update
    flash.now[:success] = "Unable to update the Feed Category"

    redirect_to feed_category_path @model
  end

  def successful_destroy
    flash.now[:success] = "Successfully deleted the Feed Category"

    redirect_to feed_categories_path
  end

  def unsuccessful_destroy
    flash.now[:success] = "Unable to delete the Feed Category"
  end
end
