class ApplicationController < ActionController::Base
  before_action :vueless

  def vueless
    @vueless = true
  end

  def redirect_to(url)
    respond_to do |format|
      format.js { tubolinks_redirect(url) }
      format.json { super(url, format: :json) }
      format.html { super(url) }
    end
  end

  def tubolinks_redirect(url)
    render js: "Turbolinks.visit('#{URI.parse(url).path}')"
  end

  def render_html_form(model = @model, error: false)
    status = nil
    status = :unprocessable_entity if error

    render partial: "form", status: status, locals: { model: model }
  end

  def render_json_errors(model = @model)
    render(
      json: model.errors.messages.to_json,
      status: :unprocessable_entity,
    )
  end

  def render_json_success
    render json: { status: :success }
  end
end
