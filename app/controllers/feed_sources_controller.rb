# frozen_string_literal: true

class FeedSourcesController < ApplicationController
  include CrudHelper
  include FeedSourceHelper

  def index
    load_collection
    load_new_model
  end

  def new
    load_new_model
  end

  def edit
    load_model
  end

  def show
    load_model
  end

  def create
    @model = FeedSource.new(resource_params)

    @model.save ? successful_create : unsuccessful_create
  end

  def update
    load_model

    @model.assign_attributes(resource_params)

    @model.save ? successful_update : unsuccessful_update
  end

  def destroy
    load_model

    @model.destroy ? successful_destroy : unsuccessful_destroy
  end

  private

  def resource_params
    params.require(:feed_source).permit(:url)
  end

  def load_model
    @model = FeedSource.find(params[:id])
  end

  def load_collection
    @collection = FeedSource.all
  end

  def load_new_model
    @model = FeedSource.new
  end

  def successful_create
    flash[:success] = create_success_msg

    redirect_to feed_sources_url
  end

  def unsuccessful_create
    respond_to do |format|
      format.js { render_html_form(error: true) }
      format.json { render_json_errors }
      format.html { create_error }
    end
  end

  def successful_update
    respond_to do |format|
      format.js { render_html_form }
      format.json { render_json_success }
      format.html { update_success }
    end
  end

  def update_success
    flash.now[:success] = update_success_msg

    render :edit
  end

  def unsuccessful_update
    respond_to do |format|
      format.js { render_html_form(error: true) }
      format.json { render_json_errors }
      format.html { update_error }
    end
  end

  def successful_destroy
    flash[:success] = destroy_success_msg

    redirect_to feed_sources_url
  end

  def unsuccessful_destroy
    respond_to do |format|
      format.js { render_html_form(error: true) }
      format.html { destroy_error }
    end
  end

  def create_error
    flash.now[:error] = create_error_msg

    render_index
  end

  def destroy_error
    flash.now[:error] = destroy_error_msg

    render_index
  end

  def render_index
    load_collection

    render :index
  end

  def update_error
    flash.now[:error] = update_error_msg

    render :edit
  end
end
