class FeedSource < ApplicationRecord
  validates :url, uniqueness: true
  validate :feed_source_validation

  private

  def feed_source_validation
    parser = RssParser.new

    parser.parse(self)
  end
end
