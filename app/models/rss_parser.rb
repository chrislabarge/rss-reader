class RssParser
  require "rss"
  require "open-uri"

  def parse(feed_source)
    url = feed_source.url

    begin
      content = read_url(url)
    rescue StandardError => e
      feed_source.errors.add(:url, "Not a valid URL")
      return "URL Error: " + e.to_s
    end

    begin
      feed = RSS::Parser.parse(content, false)
    rescue StandardError => e
      log_error(feed_source)
      return "Parse Error: " + e.to_s
    end

    if feed.nil?
      log_error(feed_source)
    else
      feed.items.each { |item| format_item item }
    end
  end

  private

  def read_url(url)
    return open(url).read if valid_url?(url)

    raise StandardError.new("Invalid URL")
  end

  def valid_url?(url)
    uri = URI::parse(url)

    uri && !uri.host.nil?
  end

  def format_item(item)
    # "Title: #{item.title}"
    # "Date: #{item.pubDate}"
    # item.link
    # item.description
    item
  end

  def log_error(model)
    model.errors.add(:url, "Unable to read Feed from URL")
  end
end
