class FeedCategory < ApplicationRecord
  has_many :user_feed_sources
  has_many :feed_sources, through: :user_feed_sources
end
