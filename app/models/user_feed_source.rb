# frozen_string_literal: true

class UserFeedSource < ApplicationRecord
  belongs_to :feed_categories
  belongs_to :feed_sources

  validates :name, uniqueness: { scope: :category_id }
end
