import { Controller } from "stimulus";

export default class extends Controller {
  static targets = ["temp"]

  connect() {
    if (this.hasTempTarget) {
      this.processTempAlert(this.tempTarget);
    }
  }

  processTempAlert(alert) {
    setTimeout(() => {
      this.closeAlert(alert);
      // TODO: refactor this so it waits for ".visible?" - to prevent from dead time
      setTimeout(() => {
        this.removeAlert(alert);
      }, 1000);
    }, 3000);
  }

  closeAlert(alert) {
    alert.querySelector(".close").click();
  }

  removeAlert(alert) {
    alert.remove();
  }
}
