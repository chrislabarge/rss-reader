import { SharedController } from "./shared_controller";

export default class extends SharedController {
  static targets = ["spinnerBtn", "autoSubmit"]

  initialize() {
    this.setExisting();
    this.submittedCount = 0;
    this.enable();
  }

  connect() {
    if (this.existingForm) {
      this.existingForm.replaceWith(this.element);
    }

    if (this.hasSpinnerBtnTarget) {
      this.initSpinners();
    }
  }

  autoSubmit(event) {
    clearTimeout(this.timer);
    const count = this.submittedCount;

    if (event.currentTarget.value !== "") {
      this.timer = setTimeout(() => {
        if (!this.disabled && this.submittedCount === count) {
          if (this.hasSpinnerBtnTarget) {
            this.addSpinnerTo(this.spinnerBtnTarget);
          }

          Rails.fire(this.element, 'submit');
          this.disable();
        }
      }, 2000);
    }
  }

  submitComplete() {
    this.submittedCount += 1;
    this.removeSpinners();
    this.enable();
  }

  enable() {
    this.disabled = false;
  }

  disable() {
    this.disabled = true;

    this.element
      .querySelector("[type='submit']")
      .disable = true;
  }

  error() {
    const alert = { error: this.data.get("error") };

    this.renderAlert(alert);
  }

  success() {
    const alert = { success: this.data.get("success") };

    this.renderAlert(alert);
  }

  replace(event) {
    const [data] = event.detail;

    this.element.outerHTML = data;
  }

  setExisting() {
    const existingForm = document.getElementById(this.element.id);

    if (existingForm !== this.element) {
      this.existingForm = existingForm;
    }
  }

  initSpinners() {
    this.spinnerBtnTargets.forEach((btn) => {
      btn.addEventListener("click", () => {
        this.addSpinnerTo(btn);
      });
    });
  }

  renderErrors(event) {
    const [data] = event.detail;

    const fields = Object.keys(data);

    fields.forEach((field) => {
      this.renderFieldError(field, data[field]);
    });
  }

  addSpinnerTo(el) {
    el.classList.add("spinner");
  }

  removeSpinners() {
    this.element.querySelectorAll(".spinner").forEach((el) => {
      el.classList.remove("spinner");
    });
  }
}
