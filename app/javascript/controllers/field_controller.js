import { SharedController } from "./shared_controller";

export default class extends SharedController {
  static targets = ["error"]

  connect() {
    this.initFieldError();
  }

  initFieldError() {
    this.errorTargets.forEach((error) => {
      const input = error.closest(".field-section").querySelector("input");

      input.addEventListener("input", () => {
        if (input.value === "") {
          error.remove();
        }
      });
    });
  }
}
