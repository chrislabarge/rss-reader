module CrudHelper
  def create_error_msg(resource = current_resource)
    resource_msg(resource, :create, :error)
  end

  def create_success_msg(resource = current_resource)
    resource_msg(resource, :created, :success)
  end

  def update_success_msg(resource = current_resource)
    resource_msg(resource, :updated, :success)
  end

  def update_error_msg(resource = current_resource)
    resource_msg(resource, :update, :error)
  end

  def destroy_success_msg(resource = current_resource)
    resource_msg(resource, :deleted, :success)
  end

  def destroy_error_msg(resource = current_resource)
    resource_msg(resource, :delete, :error)
  end

  def resource_msg(resource, action, type)
    prefix = case type.to_sym
             when :success
               "Successfully"
             when :error
               "Unable to"
             end

    "#{prefix} #{action} the #{resource.model_name.human.titleize}"
  end
end
