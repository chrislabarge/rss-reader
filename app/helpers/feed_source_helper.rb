module FeedSourceHelper
  include ApplicationHelper

  def field_wrapper
    "flex items-center border-b-2 border-pink-200 focus-within:border-purple-500 py-2"
  end

  def txt_field
    "appearance-none bg-transparent border-none w-full text-purple-100 mr-3 py-1 px-4 text-3xl leading-tight focus:outline-none"
  end

  def submit_btn
    "flex-shrink-0 bg-purple-500 hover:bg-purple-700 border-purple-500 hover:border-purple-700 text-lg border-4 text-purple-100 py-2 px-4 rounded"
  end
end
