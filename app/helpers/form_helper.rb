module FormHelper
  def form_options(custom = {})
    options = default_form_options(custom)

    options.merge(custom)
  end

  def default_form_options(custom)
    resource = custom[:model] || current_resource
    format = custom[:format].try(:to_sym)

    options = {
      model: resource,
      local: js_disabled?,
      class: "w-full max-w-2xl",
      data: {
        controller: :form,
        "form-error" => form_msg(resource, :error),
        "form-success" => form_msg(resource, :success),
        action: default_form_actions.join(" "),
      },
    }

    set_action_options(options, custom[:method], format)
  end

  def set_action_options(options, request_method, format)
    if update?(options[:model], request_method)
      options[:data][:action] += " ajax:success->form#replace" unless format == :json
      options[:data][:action] += " ajax:success->form#success"
    end

    options[:data][:action] <<
      if format == :json
        " ajax:complete->form#submitComplete"
      else
        " ajax:error->form#replace"
      end

    # This is not an action option (function name not correct)
    options[:id] = resource_form_id(options[:model], request_method)

    options
  end

  def update?(resource, request_method)
    request_method != :delete && resource.persisted?
  end

  def default_form_actions
    [
      "submit->form#disable",
      "ajax:error->form#error",
      "ajax:error->form#renderErrors",
    ]
  end

  def form_msg(resource, type)
    send("#{resource_action(resource)}_#{type}_msg", resource)
  end

  def resource_action(resource)
    resource.persisted? ? :update : :create
  end

  def field_wrapper_for(object, attr, &block)
    render "shared/field_wrapper", errors: object.errors.messages[attr], &block
  end

  def auto_submit_format
    js_disabled? ? :html : :json
  end
end
