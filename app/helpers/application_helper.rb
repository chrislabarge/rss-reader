module ApplicationHelper
  def js_disabled?
    ENV["JS_DISABLED"] == "true"
  end

  def current_resource
    # rubocop:disable Rails/HelperInstanceVariable
    @model
    # rubocop:enable Rails/HelperInstanceVariable
  end

  def resource_id(resource, prefix: nil, suffix: nil)
    return id = dom_id(resource, prefix) if suffix.blank?

    id + "_#{suffix}"
  end

  def resource_form_id(resource, action = nil)
    action ||= (resource.persisted? ? :edit : :new)

    resource_id(resource, prefix: action)
  end
end
