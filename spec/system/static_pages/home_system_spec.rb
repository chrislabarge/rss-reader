require "rails_helper"
require "support/helpers/feed_helper.rb"

RSpec.describe "Home Page", type: :system do
  include FeedHelper

  it "Displays the Home Page content" do
    visit "/"

    expect(page).to have_text("RSS Reader")
  end
end
