require "rails_helper"
require "support/helpers/alert_helper.rb"

RSpec.describe "Authentication:Destroy User Session", type: :system do
  include AuthenticationHelper
  include AlertHelper

  let(:user) { create(:user) }

  before do
    authenticate_user(user)

    visit root_path
  end

  it "user ends the current session" do
    sign_out

    expect_toast_alert(successful_message)
  end

  def successful_message
    authentication_messages.dig("registrations", "signed_out")
  end
end
