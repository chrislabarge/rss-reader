require "rails_helper"
require "support/helpers/alert_helper.rb"

RSpec.describe "Authentication:Registration", type: :system do
  include SystemHelper
  include AuthenticationHelper
  include AlertHelper

  before do
    visit new_user_registration_path
  end

  it "the register successfully fills out the form" do
    user_count = User.count

    fill_in_form

    submit_form

    expect_toast_alert(successful_message)

    expect(User.count).to eq user_count + 1
  end

  it "register provides a password that is too short" do
    short_password = "foo"

    fill_in_form password: short_password

    submit_form

    expect(page).to have_text(/too short/i)
  end

  it "register provides a non matching password" do
    non_matching_password = "foobar"

    fill_in_form
    fill_in "Password", with: non_matching_password

    submit_form

    expect(page).to have_text(/doesn't match Password/i)
  end

  def successful_message
    authentication_messages.dig("registrations", "signed_up")
  end
end
