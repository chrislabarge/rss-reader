require "rails_helper"
require "support/helpers/alert_helper.rb"

RSpec.describe "Authentication:Create User Session", type: :system do
  include SystemHelper
  include AuthenticationHelper
  include AlertHelper

  context "when a user successfully signs in" do
    let(:user) { create(:user) }

    it "logs a user in" do
      visit new_user_session_path

      fill_in_form(email: user.email, password: user.password)

      submit_form

      expect_toast_alert(successful_message)
    end

    it "with remember me checked, it permanently logs in the user", js: true do
      visit new_user_session_path

      fill_in_form(email: user.email, password: user.password)

      check("Remember me")

      submit_form

      expect(page.driver.cookies["remember_user_token"]).to be_present
      expect_toast_alert(successful_message)
    end
  end

  it "user signs in with an invalid email" do
    invalid_email = "invalid@email"
    visit new_user_session_path

    fill_in_form(email: invalid_email)

    submit_form

    expect_toast_alert(unsuccessful_message)
  end

  it "user signs in with wrong email" do
    wrong_email = "invalid@email"
    user = create(:user)

    visit new_user_session_path

    fill_in_form(email: wrong_email, password: user.password)

    submit_form

    expect_toast_alert(unsuccessful_message)
  end

  it "user signs in with wrong password" do
    wrong_password = "foobar"
    user = FactoryBot.create(:user)

    visit new_user_session_path

    fill_in_form(email: user.email, password: wrong_password)

    submit_form

    expect_toast_alert(unsuccessful_message)
  end

  def successful_message
    "Signed in successfully"
  end

  def unsuccessful_message
    "Invalid Email or password"
  end
end
