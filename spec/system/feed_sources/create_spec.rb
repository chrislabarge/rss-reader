require "rails_helper"
require "support/shared_examples_for_feed_sources.rb"
require "support/helpers/feed_helper.rb"
require "support/helpers/alert_helper.rb"

RSpec.describe "Create Feed Source", type: :system do
  include FeedHelper
  include AlertHelper

  before do
    mock_rss_feed
  end

  let!(:model) { build_stubbed(:feed_source) }
  let!(:feed_source_count) { FeedSource.count }

  it_behaves_like(
    "A FeedSource",
    :feed_sources_path,
    :create_error_msg,
    :create,
  )

  it "creates a new Feed Source" do
    msg = create_success_msg(model)

    visit new_feed_source_path

    submit_completed_form(model)

    expect_toast_alert(msg)
    expect(FeedSource.count).to eq feed_source_count + 1
  end

  it "creates a new Feed Source", js: true do
    msg = create_success_msg(model)

    visit new_feed_source_path

    submit_completed_form(model)

    expect_toast_alert(msg)
    expect(FeedSource.count).to eq feed_source_count + 1
  end
end
