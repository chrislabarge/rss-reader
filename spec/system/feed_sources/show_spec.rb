require "rails_helper"
require "support/helpers/feed_helper.rb"

RSpec.describe "Show Feed Source", type: :system do
  include FeedHelper

  before do
    mock_rss_feed
  end

  it "shows a Feed Sources data" do
    model = create(:feed_source)

    visit feed_source_path model

    expect(page).to have_content model.url
  end
end
