require "rails_helper"
require "support/helpers/feed_helper.rb"
require "support/helpers/alert_helper.rb"

RSpec.describe "Destroy Feed Source", type: :system do
  include FeedHelper
  include AlertHelper

  context "when chosen to be deleted" do
    before do
      mock_rss_feed
    end

    let!(:model) { create(:feed_source) }
    let!(:feed_source_count) { FeedSource.count }
    let!(:msg) { destroy_success_msg(model) }

    it "is destroyed from the index page" do
      visit feed_sources_path

      click_on "Delete"

      expect_toast_alert(msg)
      expect(FeedSource.count).to eq feed_source_count - 1
    end

    it "is destroyed from the show page" do
      msg = destroy_success_msg(model)
      visit feed_source_path model

      click_on "Delete"

      expect_toast_alert(msg)
      expect(FeedSource.count).to eq feed_source_count - 1
    end
  end
end
