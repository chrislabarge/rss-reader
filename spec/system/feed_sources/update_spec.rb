require "rails_helper"
require "support/shared_examples_for_feed_sources.rb"
require "support/helpers/feed_helper.rb"
require "support/helpers/alert_helper.rb"

RSpec.describe "Edit Feed Source", type: :system do
  include FeedHelper
  include AlertHelper

  before do
    mock_rss_feed
  end

  let!(:model) { create(:feed_source) }
  let!(:feed_source_count) { FeedSource.count }

  it_behaves_like(
    "A FeedSource",
    :edit_feed_source_path,
    :update_error_msg,
    :update,
  )
  it "updates a Feed Source" do
    msg = update_success_msg(model)
    model.url = another_valid_url

    visit edit_feed_source_path model

    submit_completed_form model

    expect_toast_alert(msg)

    expect(FeedSource.count).to eq feed_source_count
    expect(model.reload.url).to eq another_valid_url
  end

  it "updates a Feed Source with JS request", js: true do
    msg = update_success_msg(model)
    model.url = another_valid_url

    visit edit_feed_source_path model

    submit_completed_form model

    expect_toast_alert(msg)

    expect(FeedSource.count).to eq feed_source_count
    expect(model.reload.url).to eq another_valid_url
  end
end
