require "rails_helper"
require "support/helpers/feed_helper.rb"

RSpec.describe "Feed Source Index", type: :system do
  include FeedHelper

  before do
    mock_rss_feed
  end

  it "Displays the Feed Sources" do
    model = create(:feed_source)

    visit feed_sources_path

    expect(page).to have_content(model.url)
  end
end
