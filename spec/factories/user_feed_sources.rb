FactoryBot.define do
  factory :user_feed_source do
    feed_categories { nil }
    feed_sources { nil }
  end
end
