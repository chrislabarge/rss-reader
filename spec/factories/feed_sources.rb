FactoryBot.define do
  factory :feed_source do
    url { FeedHelper.valid_url }
  end
end
