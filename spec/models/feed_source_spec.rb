require "rails_helper"
require "support/helpers/feed_helper.rb"

RSpec.describe FeedSource, type: :model do
  include FeedHelper

  describe "url validation" do
    it "is invalid when the url is invalid" do
      subject = build_stubbed(:feed_source, url: invalid_url)

      actual = subject.valid?

      expect(actual).to eq false
    end
  end

  describe "url uniqueness validation" do
    let(:existing) { create(:feed_source) }
    let(:unique_url) { another_valid_url }

    before do
      mock_rss_feed
    end

    it "is invalid when the url is duplicate" do
      source = FeedSource.new(url: existing.url)

      actual = source.valid?

      expect(actual).to eq false
      expect(source.errors.messages[:url].last).to include("taken")
    end

    it "is validate when the url is unique" do
      mock_rss_feed

      source = FeedSource.new(url: unique_url)

      actual = source.valid?

      expect(actual).to eq true
    end
  end
end
