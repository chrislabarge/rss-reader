module AuthenticationHelper
  def authenticate_user(user = nil, with_driver: false)
    user ||= create(:user)

    if with_driver
      visit new_user_session_path

      fill_in_form(email: user.email, password: user.password)

      submit_sign_in_form
    else
      # This allows for successful load of user.settings
      # And any initializers
      user = User.find(user.id)

      login_as(user, scope: :user)
    end

    user
  end

  def fill_in_form(email: nil, password: nil)
    email ||= Faker::Internet.email
    password ||= "thisIsaPassword72"

    fill_in "Email", with: email
    fill_in "Password", with: password

    confirmation_label_text = "Password confirmation"

    if page.has_css?("label", text: confirmation_label_text)
      fill_in confirmation_label_text, with: password
    end
  end

  def sign_out
    click_on "Sign Out"
  end

  def authentication_messages
    @devise_messages ||= YAML.load_file("config/locales/devise.en.yml")

    @devise_messages.dig("en", "devise")
  end
end
