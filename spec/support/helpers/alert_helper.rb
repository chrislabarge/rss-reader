module AlertHelper
  def expect_toast_alert(text)
    expect(page).
      to have_css(".alert-toast", text: text)
  end
end
