module SystemHelper
  def find_parent_element(element)
    element.find(:xpath, "..")
  end

  def submit_form
    find("[type='submit']").click
  end
end
