require "support/mocks/feed_mock.rb"
require "support/helpers/system_helper.rb"

module FeedHelper
  include CrudHelper
  include SystemHelper

  def fill_in_form(model)
    fill_in "feed_source_url", with: model.url
  end

  def submit_completed_form(model)
    fill_in_form(model)

    submit_form
  end

  def self.valid_url
    "https://rubyweekly.com/rss"
  end

  def self.another_valid_url
    "https://www.another-url.com"
  end

  def valid_url
    FeedHelper.valid_url
  end

  def another_valid_url
    FeedHelper.another_valid_url
  end

  def invalid_url
    "NOT A URL"
  end

  def non_rss_url
    "https://example.com"
  end

  def invalid_rss_url
    "https://invalid_rss.com"
  end
end
