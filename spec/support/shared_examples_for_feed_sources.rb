RSpec.shared_examples "A FeedSource" do |path_method, expect_method, action|
  include FeedHelper

  let(:path) { send(path_method, model) }
  let(:run_expectations) { send(expect_method, model) }

  before do
    mock_rss_feed
  end

  it "validates a URL" do
    allow(model).to receive(:url) { invalid_url }

    visit path

    submit_completed_form model

    run_expectations

    expect(page).to have_content("Not a valid URL")
    expect(FeedSource.count).to eq feed_source_count
  end

  it "validates a RSS URL" do
    allow(model).to receive(:url) { non_rss_url }

    visit path

    submit_completed_form model

    run_expectations

    expect(page).to have_content("Unable to read Feed from URL")
    expect(FeedSource.count).to eq feed_source_count
  end

  it "validates correctly formated RSS Feeds" do
    allow(model).to receive(:url) { invalid_rss_url }

    visit path

    submit_completed_form model

    run_expectations

    expect(page).to have_content("Unable to read Feed from URL")
    expect(FeedSource.count).to eq feed_source_count
  end

  it "validates the URL after the user is done typing and removes the field error upon clearing the field", js: true do
    allow(model).to receive(:url) { invalid_url }

    visit path

    fill_in_form(model)

    sleep(1)

    run_expectations

    expect(page).to have_content("Not a valid URL")
    expect(FeedSource.count).to eq feed_source_count

    allow(model).to receive(:url).and_return("")

    fill_in_form(model)

    expect(page).not_to have_content("Not a valid URL")
  end

  it "does not duplicate consecutive redundant errors", js: true do
    allow(model).to receive(:url) { invalid_url }

    visit path

    fill_in_form(model)

    sleep(1)

    run_expectations

    expect(page).to have_content("Not a valid URL")

    submit_completed_form model

    expect(all(".field-section .error", text: "Not a valid URL").count).to eq 1
  end

  pending "prevents from duplicate submissions", js: true do
    submit_count = 0
    allow(model).to receive(:url) { invalid_url }
    allow_any_instance_of(FeedSourcesController).
      to receive(action.to_sym) { submit_count += 1 }

    visit path

    fill_in_form(model)

    submit_form

    pending "I can mock the request to take a tally to test against"
    # "Right now this is submitting the form twice regardless"
    # I have to come up with some JS that prevents from form submission if
    # there has been no change to the errored out field.  I wonder if I can
    # somehow send a message to the form controller from the FieldController
    # after a Error field's input has changed sense the returned response...

    submit_form

    expect(submit_count).to eq 1
    #    expect(FeedSource.count).to eq feed_source_count + 1
  end
end
