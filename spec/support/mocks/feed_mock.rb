def mock_rss_feed
  parser = RssParser.new

  allow(RssParser).to receive(:new) { parser }

  allow(parser).to receive(:read_url).with(valid_url) {
    open_valid_rss_file
  }

  allow(parser).to receive(:read_url).with(another_valid_url) {
    open_valid_rss_file
  }

  allow(parser).to receive(:read_url).with(invalid_url) {
    open("NOT A URL").read
  }

  allow(parser).to receive(:read_url).with(non_rss_url) {
    open("./spec/support/html/example.html").read
  }

  allow(parser).to receive(:read_url).with(invalid_rss_url) {
    open("./spec/support/feeds/invalid_rss.xml").read
  }
end

def open_valid_rss_file
  open("./spec/support/feeds/ruby_weekly.xml").read
end
