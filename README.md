![Logo](https://gitlab.com/chrislabarge/rss-reader/raw/master/public/logo.png)
# SS Reader

A Feedly clone, maybe more.

## Why

This is hobby app for trying out modern rails tooling.


### Tools

* Rails 6
* Turbolinks
* StimulusJS
* Vue
* Tailwind CSS
* Prettier


Features Wanted
-------------------------------------

* [ Distraction free RSS Feed Reader. ]

* [ A night theme. ]

* [ Font Selection. ]

* [ Actually, any theme/custom color someone wants.  Lets provide a color wheel. ]

* [ A way to seamlessly scroll to the next subscription. ]

* [ Typical ability to subscribe to RSS feeds. ]

* [ Personally want to be able to subscribe to many subreddits. ]

* [ UI/Workflows to be very aesthetically pleasing. ]

TECHNICAL-STEPS
---------------------------

http://www.whatisrss.com/

https://feedly.com/

I will need to come up with a model that I want to call the different subscriptions a user can have.  Feed || Subscription || ....

![Roadmap](https://gitlab.com/chrislabarge/rss-reader/raw/master/public/roadmap.png)

Contributing
-------------------

Feel free to start a ticket. Please add any additional information, or questions to tickets in the comment section.

#### Development Work-flows and Patterns
 - Server Side appended JS (old Rais remote pattern)

   Use `shared/form_error` to re-render forms. Stimulus will take car for the rest.


The Dump Yard
----------------------------------

This is a collection of URLS of different resources I have used in one way or another to develop this application.

* https://github.com/thoughtbot/factory_bot_rails
* https://github.com/stimulusjs/stimulus/issues/59
* https://medium.com/@prrrnd/upgrading-to-rails-6-and-getting-started-with-action-text-7f50aafc8631
* https://www.driftingruby.com/episodes/getting-started-with-vuejs
* https://vuejs.org/v2/guide/
* https://medium.com/@davidteren/rails-6-and-tailwindcss-getting-started-42ba59e45393
* https://github.com/jvanbaarsen/tailwindcss-rails-example
* https://gorails.com/episodes/tailwind-css-framework-with-rails
* https://www.tailwindtoolbox.com/components/alerts
* https://stephanwagner.me/only-css-loading-spinner
