Rails.application.routes.draw do
  devise_for :users, controllers: { registrations: "users/registrations" }

  root to: "static_pages#home"

  resources :feed_categories
  resources :feed_sources
end
