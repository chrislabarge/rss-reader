// config/webpack/loaders/eslint.js
const { env } = require('../development.js');

module.exports = {
  enforce: 'pre',
  test: /\.(js|vue)$/i,
  exclude: /node_modules/,
  loader: 'eslint-loader',
  options: {
    failOnError: env.NODE_ENV !== 'production',
  },
};
