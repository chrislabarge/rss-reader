class CreateUserFeedSources < ActiveRecord::Migration[6.0]
  def change
    create_table :user_feed_sources do |t|
      t.references :feed_categories, null: false, foreign_key: true
      t.references :feed_sources, null: false, foreign_key: true

      t.timestamps
    end
  end
end
