class CreateFeedCategories < ActiveRecord::Migration[6.0]
  def change
    create_table :feed_categories do |t|
      t.string :name
      t.integer :place

      t.timestamps
    end
  end
end
