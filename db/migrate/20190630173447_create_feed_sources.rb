class CreateFeedSources < ActiveRecord::Migration[6.0]
  def change
    create_table :feed_sources do |t|
      t.string :url

      t.timestamps
    end
  end
end
